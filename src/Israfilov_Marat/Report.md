# Методы программирования 2: Множества на основе битовых полей


## Цели и задачи


__Цель данной работы__ - разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

__Задачи:__    
1. Реализация класса битового поля `TBitField` согласно заданному интерфейсу.
2. Реализация класса множества `TSet` согласно заданному интерфейсу.
3. Обеспечение работоспособности тестов и примера использования.
4. Реализация нескольких простых тестов на базе Google Test.
5. Публикация исходных кодов в личном репозитории на GitHub.

## Начало работы:

### 1. Класс битового поля:

Состоит из заголовочного файла `tbitfield.h` и файла реализации `tbitfield.cpp`

#### Заголовочный файл `tbitfield.h`:

```c++
#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
  int  BitLen; // длина битового поля - макс. к-во битов
  TELEM *pMem; // память для представления битового поля
  int  MemLen; // к-во эл-тов Мем для представления бит.поля

  // методы реализации
  int   GetMemIndex(const int n) const; // индекс в pМем для бита n       (#О2)
  TELEM GetMemMask (const int n) const; // битовая маска для бита n       (#О3)
public:
  TBitField(int len);                //                                   (#О1)
  TBitField(const TBitField &bf);    //                                   (#П1)
  ~TBitField();                      //                                    (#С)

  // доступ к битам
  int GetLength(void) const;      // получить длину (к-во битов)           (#О)
  void SetBit(const int n);       // установить бит                       (#О4)
  void ClrBit(const int n);       // очистить бит                         (#П2)
  int  GetBit(const int n) const; // получить значение бита               (#Л1)

  // битовые операции
  int operator==(const TBitField &bf) const; // сравнение                 (#О5)
  int operator!=(const TBitField &bf) const; // сравнение
  TBitField& operator=(const TBitField &bf); // присваивание              (#П3)
  TBitField  operator|(const TBitField &bf); // операция "или"            (#О6)
  TBitField  operator&(const TBitField &bf); // операция "и"              (#Л2)
  TBitField  operator~(void);                // отрицание                  (#С)

  friend istream &operator>>(istream &istr, TBitField &bf);       //      (#О7)
  friend ostream &operator<<(ostream &ostr, const TBitField &bf); //      (#П4)
};
// Структура хранения битового поля
//   бит.поле - набор битов с номерами от 0 до BitLen
//   массив pМем рассматривается как последовательность MemLen элементов
//   биты в эл-тах pМем нумеруются справа налево (от младших к старшим)
// О8 Л2 П4 С2

#endif
```

#### Файл реализации `tbitfield.cpp`:

В качестве обработки ошибок используется исключения `throw`:
* throw _1_ - выбрасывается исключение в случае отрицательной длины битового поля.
* throw _2_ - выбрасывается исключение в случае выхода за границу переменной _n_.

Определен макрос с именем `BIT_TELEM` равный количеству бит в переменной типа `TELEM`

```c++
#include "tbitfield.h"
#define BIT_TELEM (sizeof(TELEM)*8)

TBitField::TBitField(int len)
{
	if (len < 0) throw 1; // "Error: negative length!"	
	BitLen = len;
	MemLen = (len / BIT_TELEM) + 1;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (n / BIT_TELEM);
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (TELEM)(1 << (n % BIT_TELEM));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n < 0 || n >= BitLen) throw 2; // "Error: incorrect bit value"
	return (pMem[GetMemIndex(n)] & GetMemMask(n)) ? 1 : 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (*this != bf)
	{
		delete[] pMem;
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	}

	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	TELEM value = 0;
	if (BitLen == bf.BitLen)
	{
		value = 1;
		for (int i = 0; i < bf.MemLen; i++)
			if (pMem[i] != bf.pMem[i]) { value = 0; break; }
	}
	return value;
}

int TBitField::operator !=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	int Len;
	(BitLen > bf.BitLen) ? Len = BitLen : Len = bf.BitLen;
	TBitField tmp(Len);
	for (int i = 0; i < tmp.MemLen; i++)
		tmp.pMem[i] = pMem[i] | bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	int Len;
	(BitLen > bf.BitLen) ? Len = BitLen : Len = bf.BitLen;
	TBitField tmp(Len);
	for (int i = 0; i < tmp.MemLen; i++)
		tmp.pMem[i] = pMem[i] & bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField tmp(*this);
	for (int i = 0; i < BitLen; i++)
		(tmp.GetBit(i) == 0) ? tmp.SetBit(i) : tmp.ClrBit(i);

	return tmp;
}

// ввод/вывод

istream &operator >> (istream &istr, TBitField &bf) // ввод
{
	char value;
	for (int i = 0; i < bf.BitLen; i++)
	{
		istr >> value;
		if (value != '0' && value != '1') break;
		(value == '1') ? bf.SetBit(i) : bf.ClrBit(i);
	}

	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		ostr << bf.GetBit(i);

	return ostr;
}
```

### 2. Класс множества:

Состоит из заголовочного файла `tset.h` и файла реализации `tset.cpp`

Класс __TSet__ реализован на основе класса __TBitField__

#### Заголовочный файл `tset.h`:

```c++
#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet
{
private:
  int MaxPower;       // максимальная мощность множества
  TBitField BitField; // битовое поле для хранения характеристического вектора
public:
  TSet(int mp);
  TSet(const TSet &s);       // конструктор копирования
  TSet(const TBitField &bf); // конструктор преобразования типа
  operator TBitField();      // преобразование типа к битовому полю
  // доступ к битам
  int GetMaxPower(void) const;     // максимальная мощность множества
  void InsElem(const int Elem);       // включить элемент в множество
  void DelElem(const int Elem);       // удалить элемент из множества
  int IsMember(const int Elem) const; // проверить наличие элемента в множестве
  // теоретико-множественные операции
  int operator== (const TSet &s) const; // сравнение
  int operator!= (const TSet &s) const; // сравнение
  TSet& operator=(const TSet &s);  // присваивание
  TSet operator+ (const int Elem); // объединение с элементом
                                   // элемент должен быть из того же универса
  TSet operator- (const int Elem); // разность с элементом
                                   // элемент должен быть из того же универса
  TSet operator+ (const TSet &s);  // объединение
  TSet operator* (const TSet &s);  // пересечение
  TSet operator~ (void);           // дополнение

  friend istream &operator>>(istream &istr, TSet &bf);
  friend ostream &operator<<(ostream &ostr, const TSet &bf);
};
#endif
```

#### Файл реализации `tset.cpp`:

```c++
#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
    return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	if (this != &s)
	{
		MaxPower = s.MaxPower;
		BitField = s.BitField;
	}
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
    return ((MaxPower == s.MaxPower) && (BitField == s.BitField));
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet tmp = BitField | s.BitField;
	return tmp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet tmp(*this);
	tmp.InsElem(Elem);
	return tmp;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet tmp(*this);
	tmp.DelElem(Elem);
	return tmp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet tmp = BitField & s.BitField;
	return tmp;
}

TSet TSet::operator~(void) // дополнение
{
	TSet tmp = ~BitField;
	return tmp;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int value;
	char ch;
	istr >> ch; // Ввод '{'
	for (int i = 0; i < s.MaxPower; i++)
	{
		istr >> value;
		s.InsElem(value);
		istr >> ch; //Ввод ','
		if (ch == '}') break;		
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr << '{';
	for (int i = 0; i < s.MaxPower; i++)
		if (s.IsMember(i))
			ostr << i << ',';		
	ostr << '\b' << '}' << endl;
	return ostr;	
}
```

### 3. Работоспособность тестов и примеры использования:

#### 3.1 Тесты:

Прохождение тестов для класса __TBitField__:

![](http://s019.radikal.ru/i606/1610/2f/b012cefbeabb.png)

Все тесты пройдены успешно!

Прохождение тестов для класса __TSet__:

![](http://s46.radikal.ru/i111/1610/53/32b74ea88191.png)

Все тесты пройдены успешно!

#### 3.2 Пример использования:

В качестве примера использования классов __TBitField__ и __TSet__ был взят алгоритм поиска простых чисел от 2 до `n`, называемое "Решето Эратосфена".

![](http://s017.radikal.ru/i411/1610/d4/fb857c6c9482.png)

В данном примере верхняя граница `n` = 70.
Полученный результат верный! В чем легко убедиться, взглянув на таблицу простых чисел:

![](http://shkolo.ru/i/tablica%20prostyh%20chisel.gif)

### 4. Реализация простых тестов:

Самостоятельно была проверена возможность использовать несколько операций в одну строчку(например `bf1 | bf2 | bf3` и `set1 + set2 + set3`).

```c++
#include "tbitfield.h"
#include "tset.h"
using namespace std;

int main()
{
	try
	{
		TBitField bf1(5), bf2(5), bf3(5), bf4(5);
		TSet set1(5), set2(5), set3(5), set4(5);
		cout << "bf1 = 10000; bf2 = 01000; bf3 = 00011;" << endl;
		bf1.SetBit(0);
		bf2.SetBit(1);
		bf3.SetBit(3);
		bf3.SetBit(4);
		bf4 = bf1 | bf2 | bf3;
		cout << "bf4 = bf1 | bf2 | bf3 = " << bf4 << endl << endl;

		cout << "set1 = {0}; set2 = {1}; set3 = {3,4};" << endl;
		set1.InsElem(0);
		set2.InsElem(1);
		set3.InsElem(3);
		set3.InsElem(4);
		set4 = set1 + set2 + set3;
		cout << "set4 = set1 + set2 + set3 = " << set4 << endl;
	}
	catch(int err)
	{
		if(err == 1)
			cout << "Error: negative length!" << endl;
		if(err == 2)
			cout << "Error: incorrect bit value!" << endl;
	}

	return 0;
}
```
![](http://s019.radikal.ru/i614/1610/37/e23a6e8df549.png)

Тесты пройдены, результаты оказались верны!

# Вывод:

В данной работе были реализованы два класса:
* __TBitField__ для работы с битовыми полями.
* __TSet__ для работы с множеством объектов. Причем __TSet__ был реализован на основе класса __TBitField__.

Так же впервые была использована система тестирования Google Test и реализованны некоторые свои тесты.
Благодаря тестам было исправлены некоторые ошибки в классах, до их появления непосредственно во время эксплуатации этих классов.
Это дает уверенность в том что классы реализованы правильно и при их использовании, программы будут корректно выдавать результат.
